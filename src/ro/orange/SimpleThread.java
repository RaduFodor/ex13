package ro.orange;

public class SimpleThread extends Thread {
    public SimpleThread(String name) {      // constructor with String type input
        super(name);                        // thread name
    }
    public void run(){
        for (int i = 1; i < 10; i++) {
            System.out.println("Loop #" + i + ": " + getName());
            try {
                sleep((int) (Math.random() * 1000));
            }
            catch (InterruptedException e) {
            }
        }
        System.out.println("DONE! " + getName());
    }
}
